import { Routes } from '@angular/router';
import { GalleryComponent } from './gallery';
import { UploadComponent } from './upload';
import { NoContentComponent } from './no-content';

import { DataResolver } from './app.resolver';

export const ROUTES: Routes = [
  { path: '',      component: GalleryComponent},
  { path: 'upload', component: UploadComponent },
  // { path: 'detail', loadChildren: './+detail#DetailModule'},
  // { path: 'barrel', loadChildren: './+barrel#BarrelModule'},
  { path: '**',    component: NoContentComponent },
];
