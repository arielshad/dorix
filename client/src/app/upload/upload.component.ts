import {
  Component,
  OnInit
} from '@angular/core';



import { AppState } from '../app.service';
import { Title } from './title';

import {
  Ng4FilesService,
  Ng4FilesConfig,
  Ng4FilesStatus,
  Ng4FilesSelected
} from 'angular4-files-upload';


import {HttpClient} from '@angular/common/http';


@Component({
  /**
   * The selector is what angular internally uses
   * for `document.querySelectorAll(selector)` in our index.html
   * where, in this case, selector is the string 'upload'.
   */
  selector: 'upload',  // <upload></upload>
  /**
   * We need to tell Angular's Dependency Injection which providers are in our app.
   */
  providers: [
    Title
  ],
  /**
   * Our list of styles in our component. We may add more to compose many styles together.
   */
  styleUrls: [ './upload.component.css' ],
  /**
   * Every Angular template is first compiled by the browser before Angular runs it's compiler.
   */
  templateUrl: './upload.component.html'
})
export class UploadComponent implements OnInit {

  private configImage: Ng4FilesConfig = {
    acceptExtensions: ['jpg', 'jpeg', 'png'],
    maxFilesCount: 1
  };

  public selectedFiles;
  public fileNames;

  public upload(){
    console.log(this.selectedFiles);
    let data = new FormData();
    this.selectedFiles.files.map(file=>{
      data.append('image', file);
    })


    this.http.post('http://18.195.93.114:3030', data).subscribe(data => {
      console.log(data);
    })

    /*this.http.get('/api/items').subscribe(data => {
     // Read the result field from the JSON response.
     this.results = data['results'];
     });*/


    this.selectedFiles = null;
    this.fileNames  = null;
    return;
  }

  public filesSelect(selectedFiles: Ng4FilesSelected): void {
    if (selectedFiles.status !== Ng4FilesStatus.STATUS_SUCCESS) {
      return;

      // Hnadle error statuses here
    }else{
      console.log(selectedFiles);
      // console.log("status", selectedFiles.status, Ng4FilesStatus.STATUS_SUCCESS );
    }

    this.selectedFiles = selectedFiles;
    this.fileNames =  Array.from(selectedFiles.files).map(file => file.name);
  }
  /**
   * Set our default values
   */
  public localState = { file: '' };
  /**
   * TypeScript public modifiers
   */
  constructor(
    public appState: AppState,
    public title: Title,
    private ng4FilesService: Ng4FilesService,
    private http: HttpClient
  ) {}

  public ngOnInit() {
    // this.ng4FilesService.addConfig(this.configImage, 'my-image-config');


    /*this.http.get('/api/items').subscribe(data => {
      // Read the result field from the JSON response.
      this.results = data['results'];
    });*/

    // console.log('hello `Home` component');
    /**
     * this.title.getData().subscribe(data => this.data = data);
     */

  }

}
