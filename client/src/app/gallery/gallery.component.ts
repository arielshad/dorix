import {
  Component,
  OnInit
} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AppState } from '../app.service';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'gallery',
  styles: [``],
  templateUrl: './gallery.component.html'
})
export class GalleryComponent implements OnInit {

  public localState: any;
  constructor(
    public route: ActivatedRoute,
    public appState: AppState,
    private http: HttpClient
  ) {}

  public remove = (idx) =>{
    let id = this.appState.state.gallery[idx]._id;

    this.http.post(`http://18.195.93.114:3030/delete`, {id}, { responseType: 'text' }).subscribe((res)=>{
      console.log("the file was delete from server");
    })

    this.appState.state.gallery.splice(idx,1);

  }

  public ngOnInit() {
    this.http.get(`http://18.195.93.114:3030`).subscribe(data => {
      this.appState.state.gallery = data;

    })
  }


}
