**DORIX Angular Gallery App**

*Basic angular 4 gallery*

---

## TODO:

Setup infrastructure: 
Register to AWS EC2 to get a free tier account (you can use a micro instance for free).
Create an  Ubuntu instance (version 16)
Install mongoDB
Install GIT
Install NodeJS
Create S3 bucket.

Register to Bitbucket and create repository for your code. 
Write a user gallery AngularJS app that holds two pages:
Main page - display a list of images in the gallery, each image can be removed. Upload new image button will lead to the second screen.
Upload image - file upload.
The NodeJS server will Upload the images to S3 and use mongoDB to store the images references. The server will expose an API to get images, upload image and remove image.