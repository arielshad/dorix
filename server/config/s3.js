require('dotenv').load() // load env file
const s3 = require('s3');
const client = s3.createClient({
  maxAsyncS3: 20,     // this is the default
  s3RetryCount: 3,    // this is the default
  s3RetryDelay: 1000, // this is the default
  multipartUploadThreshold: 20971520, // this is the default (20 MB)
  multipartUploadSize: 15728640, // this is the default (15 MB)
  s3Options: {
    accessKeyId: process.env.AWS_ACC_KEY_ID,
    secretAccessKey: process.env.AWS_SERCRET_KEY,
    region: process.env.REGION,
    Bucket: process.env.BUCKET
  },
});

module.exports = client;

