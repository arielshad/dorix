/**
 * Created by arielshad on 20/11/2017.
 */
require('dotenv').load() // load env file
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

mongoose.connect(process.env.MONGO_DB, {useMongoClient: true});

const galleryImage = new Schema({
    path:  String,
    fileName:  String
});

const GalleryImage = mongoose.model('GalleryImage', galleryImage);

module.exports = GalleryImage;