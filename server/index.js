"use strict";
require('dotenv').load() // load env file

const express = require('express')
const app = express()
const AWS = require('aws-sdk');
const formData = require("express-form-data");
const fs = require('fs')
const bodyParser = require('body-parser')
const cors = require('cors')
const GalleryImage = require("./models")

app.use(cors({
    origin:
        [
            'http://localhost',
            'http://localhost:3000',
            'http://18.195.93.114:3030',
            'http://18.195.93.114:3000',
            'http://18.195.93.114:8080'
        ]
}))
app.use(bodyParser.json({limit: '5mb'}));
app.use(bodyParser.urlencoded({extended: true, limit: '5mb'}));
// formData middleware to fix req.files
app.use(formData.parse({autoFiles: true}));

//init s3 from env
const s3 = new AWS.S3({
    aws_access_key_id: process.env.AWS_ACCESS_KEY,
    aws_secret_access_key: process.env.AWS_SECRET_KEY
});

app.get('/', (req, res) => {
    try {
        GalleryImage.find({}, (err, result)=>{
            return res.send(JSON.stringify(result))
        });
    } catch (err) {
        throw err;
    }

})

app.post('/delete',(req, res) => {
    if(!req.body.id){
        return res.sendStatus(400)
    }


    GalleryImage.find({_id:req.body.id}, (err, result)=>{
        if(err || result.length === 0)
            return res.sendStatus(400)


        let params = {
            Bucket: process.env.BUCKET,
            Delete: {
                Objects: [{
                    Key: result[0].fileName
                }]
            }
        };

        s3.deleteObjects(params, function(err) {
            if (err) {
                throw new Error(err)
            }
        });

        GalleryImage.remove({_id:req.body.id}, (err)=>{
            if(err)
                throw new Error(err)
            return res.sendStatus(200)
        })


    })


})

// basic upload route
//TODO: validate file type
app.post('/', (req, res) => {
	let reqFile = req.files ? req.files['image'] : null;
	
	if(!reqFile){
		return res.sendStatus(400)
	}

	reqFile =  Array.isArray(reqFile)? reqFile : [reqFile]

    let numOfFiles = reqFile.length;

    reqFile.map(file=>{

        fs.readFile(file.path, (err, data) => {
            if (err) { throw err; }

            const base64data = new Buffer(data, 'binary');
            let tempFile = `${+new Date}_${file.originalFilename}`
            let params = {
                Bucket: process.env.BUCKET,
                Key: tempFile,
                Body: base64data,
                ACL: 'public-read'
            };

            s3.putObject(params, function(err, data) {
                if (err) {
                    throw new Error(err)
                }

                console.log("finish", data, numOfFiles);
                let s3Host = process.env.DEFAULT_S3_HOST || 'https://s3.eu-central-1.amazonaws.com/dorix-s3/';

                let newPic = new GalleryImage({path: `${s3Host}${tempFile}`, fileName: tempFile});
                newPic.save()


                if(numOfFiles === 0){
                    return res.sendStatus(200)
                }
            });
        })
    })


})

app.listen(process.env.PORT || 3000,
    () => console.log(`Example app listening on port ${process.env.PORT || 3000}!`)
)